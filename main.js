$(function() {
    document.querySelector('.loader').addEventListener('click', function () {

        if (document.body.className === 'loading in') {

            endAnimation('loading', 1000, function () {
                startAnimation('reveal', 2000, function () {
                    endAnimation('reveal');
                })
            })
        }
    })

    function endAnimation(name, delay, callback) {
        document.body.classList.remove('in');
        document.body.classList.add('out');
        if (delay) {
            setTimeout(function () {
                document.body.classList.remove(name, 'out');
                if (typeof callback === 'function') {
                    callback();
                }
            }, delay);
        } else {
            document.body.classList.remove(name, 'out');
        }
    }

    function startAnimation(name, delay, callback) {
        document.body.classList.add(name, 'out');

        requestAnimationFrame(function () {
            document.body.classList.remove('out');
            document.body.classList.add('in');
            if (typeof callback === 'function') {
                setTimeout(callback, delay);
            }
        })

    }




    var allImages = new Array();
    function preloadImages() {
        var allImagesLoad = [];

        for (i = 0; i < preloadImages.arguments.length; i++) {
            var onImageLoad = new Promise(function (resolve, reject) {

                allImages[i] = new Image()
                allImages[i].onload = function () {
                    resolve();
                }
                allImages[i].src = preloadImages.arguments[i];
            })

            allImagesLoad.push(onImageLoad);
        }

        return Promise.all(allImagesLoad);
    }


    // document.body.classList.add('preload');
    preloadImages(
        "images/wall.png",
        "images/vg-logo.png",
        "images/vg-logo-horizontal.png",
        "images/contact-drawer-bg.png",
        "images/splash-right.png"

    ).then(function () {
        document.body.classList.remove('preload');
    });


    /* Drawer */    
    $('[data-toggle-drawer]').bind('click', function(item) {
        document.querySelector('body').classList.toggle('drawer-active');
    });

    /* Modal */
    $('[data-toggle-modal]').bind('click', function(item) {
        var target = $(this).attr('data-toggle-modal');
        $(target).toggleClass('show');
    });


    /* Floating Label */
    $('.input-container').each(function() {
        var $container = $(this);
        var $input = $container.find('input, select, textarea');        

        if ($input.length) {
            $input.bind('focus', function() {
                $container.addClass('js-input-focused');
            });

            $input.bind('blur', function() {
                $container.removeClass('js-input-focused');
            });

            $input.bind('input change', function() {
                $container.toggleClass('js-input-has-value', !!$input.val());
            });
        }
    });


    /* Form submit */
    $('form').submit(function (e) {
        e.preventDefault();
        var $form = $(this);

        if ($form.valid()) {
            var formData = $form.serialize();
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: formData,
                success: function (data) {
                    $form.trigger('reset');
                    $form.find('input').change();
                    $('#closeDrawer').click();
                    $('#closeSuccessModal').click();
                },
                error: function (xhr, str) {
                    console.log('Error: ' + xhr.responseCode);
                }
            });
        }

        return false;
    });
});